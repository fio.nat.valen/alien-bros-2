package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import core.Animation;
import core.Sound;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.Game;
import state.State;
public class Player extends GameObject {
    public static final float WIDTH = 36, HEIGHT = 48;
    public static final float GRAVITY = 0.5f;
    public static final int MAX_GRAVITY = 10;
    public static final int SHOOT_DELAY = 40;
    
    public static final float moveSpeed = 6.0f;
    public static final float jumpValue = 15.0f;
    
    public static int PLAYER_TYPE = 0;
    
    //  player point etc
    public static int GOLD = 0;
    public static int LIFE = 3;
    public static int ATTACK = 1;
    public static boolean haveKey=false;
    
    public State state;
    private int direction = 0;
    public static int shootTime = 0;
    public boolean standing = true;
    public static boolean onBottomLadder = false;
    public static boolean onTopLadder = false;
    
    Animation[] walkAnim = new Animation[3];
    Animation[] climbAnim = new Animation[3];
    
    private Player(ObjectId id, float x, float y) {
        super(id, x, y);
        walkAnim[0] = new Animation(5, Assets.player_walk[0]);
        climbAnim[0] = new Animation(10, Assets.player_climb[0]);
        walkAnim[1] = new Animation(5, Assets.player_walk[1]);
        climbAnim[1] = new Animation(10, Assets.player_climb[1]);
        walkAnim[2] = new Animation(5, Assets.player_walk[2]);
        climbAnim[2] = new Animation(10, Assets.player_climb[2]);
        this.state = State.standing;
        this.setW((int)WIDTH);
        this.setH((int)HEIGHT);
    }
    
    //create an object of SingleObject
    private static Player instance = new Player(ObjectId.Player, 0, 0);


    //Get the only object available
    public static Player getInstance() {
        return instance;
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        shootTime -= 1;
        y += velY;
        x += velX;
        
        if ((falling || jumping) && !state.equals(State.climbing)) {
            velY += GRAVITY;
            
            if (velY > MAX_GRAVITY) {
                velY = MAX_GRAVITY;
            }
        }
        
//        if(state.getClass() == State.jumping.getClass() && getVelY()==0.5 && getVelX()!=0) {
//            state = State.standing;
//        }
        
        walkAnim[PLAYER_TYPE].runAnimation();
        climbAnim[PLAYER_TYPE].runAnimation();
        checkCollision(objects);
        
        if (standing && state == State.jumping) {
            state = State.standing;
        }

        if(LIFE<=0) {
            state = State.dead;
            velX = 0;
            velY = 0;
        }
    }
    
    public void fire() {
        try {
            Assets.balloon_pop.play();
        } catch (InterruptedException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (PLAYER_TYPE == 1) {
            ATTACK = 2;
        } else {
            ATTACK = 1;
        }
        if(shootTime<=0) {
            Bullet bullet = new Bullet(ObjectId.Bullet, this.getX(), this.getY(), direction, ObjectId.Player);
            Game.handler.addObject(bullet);
            shootTime = SHOOT_DELAY;
        }
    }
    

    @Override
    public void render(Graphics2D g) { 
        
        if (this.state == State.standing) {
            g.drawImage(Assets.player_idle[PLAYER_TYPE], (int)getX(), (int)getY(),(int)WIDTH, (int)HEIGHT, null);
        }
        else if (this.state == State.jumping) {
            standing = false;
            if (this.direction==1) {
                g.drawImage(Assets.player_jump[PLAYER_TYPE], (int)getX(), (int)getY(),(int)WIDTH, (int)HEIGHT, null);
            } else {
                g.drawImage(Assets.player_jump[PLAYER_TYPE], (int)getX()+(int)WIDTH, (int)getY(),(int)-WIDTH, (int)HEIGHT, null);
            }
        }
        else if (this.state == State.moving) {
            try {
                Assets.walking.play();
            } catch (InterruptedException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(this.getVelX()>0) {
                walkAnim[PLAYER_TYPE].drawAnimation(g, (int) x, (int) y,(int)WIDTH, (int)HEIGHT, 0);
            }
            else {
                walkAnim[PLAYER_TYPE].drawAnimation(g, (int) x, (int) y,(int)WIDTH, (int)HEIGHT, 1);
            }
            
        }
        else if (this.state == State.ducking) {
            g.drawImage(Assets.player_duck[PLAYER_TYPE], (int)getX(), (int)getY()+10,(int)WIDTH, (int)HEIGHT-10, null);
        }
        else if(this.state == State.climbing) {
            climbAnim[PLAYER_TYPE].drawAnimation(g, (int) x, (int) y, (int) WIDTH, (int) HEIGHT, 0);
        }
        else if (this.state == State.dead) {
           
            try {
                Assets.pain.play();
            } catch (InterruptedException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            g.drawImage(Assets.player_dead[PLAYER_TYPE], (int)getX(), (int)getY(),(int)WIDTH, (int)HEIGHT, null);
        }
        
        
//        Graphics2D g2d = (Graphics2D) g;
//        g2d.setColor(Color.red);
//        g2d.draw(getBounds());
//        g2d.setColor(Color.yellow);
//        g2d.draw(getBoundsTop());
//        g2d.setColor(Color.blue);
//        g2d.draw(getBoundsLeft());
//        g.setColor(Color.green);
//        g.draw(getBoundsRight());
    }
    
    @Override
    // Bottom Bound
    public Rectangle getBounds() {
        return new Rectangle((int)(x+((WIDTH/4))), (int)(y+HEIGHT/2), (int)(WIDTH/2), (int)HEIGHT/2);
    }
    
    public Rectangle getBoundsTop() {
        return new Rectangle((int)(x+((WIDTH/4))), (int)y, (int)(WIDTH/2), (int)HEIGHT/3);
    }
    
    public Rectangle getBoundsRight() {
        return new Rectangle((int)(x+((WIDTH/5)*4)), (int)(y+((int)(HEIGHT/6))), (int)WIDTH/5, (int)(HEIGHT-(HEIGHT/2)));
    }
    
    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x, (int)(y+((int)(HEIGHT/6))), (int)WIDTH/5, (int)(HEIGHT-(HEIGHT/2)));
    }
    
    public void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);
            
            if (tObject.getId() == ObjectId.TYPE_GROUND) {
                
                // When Hit at Top we need to Reposition Y and set VelY to 0
                if (getBoundsTop().intersects(tObject.getBounds())) {
                    y = tObject.getY() + tObject.getH();
                    velY = 0;
                }
                
                // When Hit at Bottom we need to Reposition Y, set VelY to 0
                // Also reset the jumping and falling
                // but once after the hit, we set it to be falling
                if (getBounds().intersects(tObject.getBounds())) {
                    y = tObject.getY() - HEIGHT;
                    velY = 0;
                    falling = false;
                    jumping = false;
                    standing = true;
                } else
                    falling = true;
                
                // When Hit at Left we need to Reposition X
                if (getBoundsRight().intersects(tObject.getBounds())) {
                    x = tObject.getX() - WIDTH;
                }
                
                // When Hit at Right we also need to Reposition X
                if (getBoundsLeft().intersects(tObject.getBounds())) {
                    x = tObject.getX() + tObject.getW();
                }
                
            }
            else if(tObject.getId() == ObjectId.Ladder) {
                Ladder l = (Ladder) tObject;
                
                if(l.getBounds().intersects(getBounds()) || l.getBoundsTop().intersects(getBounds())) {
                    if(this.getY()+HEIGHT-5+velY <= l.getY() && State.climbing == state && velY<0) {
                        state = State.standing;
                    }
                }
                
                //  X
                int playerCenterX = (int) (getX() + WIDTH / 2);
                int ladderLeft = (int) l.getX();
                int ladderRight = (int) l.getX() + l.getW() ;
                
                // Y
                
                int playerBottom = (int) (getY() + HEIGHT);
                int ladderTop = (int) l.getY();
                int ladderBottom = (int) l.getY() + l.getH();
                
                if (between(playerCenterX, ladderLeft+10, ladderRight-10)) {
                    
                    if (State.climbing != state && !l.getBoundsBottom().intersects(getBounds()) 
                            && between(playerBottom, ladderTop-5, ladderTop+5)) {
                        y = l.getY() - 48;
                        velY = 0;
                        onTopLadder = true;
                        onBottomLadder = false;
                        standing = true;
                        jumping = false;
                    } else if (getBounds().intersects(l.getBoundsBottom())) {
                        onTopLadder = false;
                        onBottomLadder = true;
                        if(state == State.climbing) {
                            state = State.standing;
                        }
                    }
                    
                    if(playerBottom < l.getY() && State.climbing == state) {
                        state = State.standing;
                        y = l.getY() - 48;
                        velY = 0;
                    }
                }
            }
        }
    }

    public int getDirection() {
        return direction;
    }

    public void setKey(boolean haveKey){
        this.haveKey = haveKey;
    }
    
    public void setDirection(int direction) {
        this.direction = direction;
    }

    // check if x between a and b
    public static boolean between(int x, int a, int b) {
        return (x>a && x<b);
    }
}
