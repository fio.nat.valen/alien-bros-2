/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;
import static objects.Player.HEIGHT;

/**
 *
 * @author Frans
 */
public class Coin extends GameObject{

    public Coin(ObjectId id, float x, float y, int w, int h) {
        super(id, x, y, w, h);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.coins, null, (int)x, (int)y);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x + (w/4), (int)y + h/4, w/2, h/2);
    }
    
    public void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);

            if (tObject.getId() == ObjectId.Player) {
                Player p = (Player) tObject;
                if(getBounds().intersects(p.getBounds())){
                    
                    Player.GOLD += 100;
                    System.out.println(Player.GOLD + " COINS");
                    Game.handler.removeObject(this);
                }
            }
        }
    }
    
}


