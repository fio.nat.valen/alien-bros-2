package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;

public class Block extends GameObject {
    int width = 32, height = 32;
    int type;
    
    public Block(ObjectId id, int type, float x, float y) {
        super(id, x, y);
        this.type = type;
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        
    }

    @Override
    public void render(Graphics2D g) {

    }
    
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, width, height);
    }
}
