/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;
import main.HUD;

/**
 *
 * @author Frans
 */
public class Shop extends GameObject{

    public Shop(ObjectId id, float x, float y, int w, int h) {
        super(id, x, y+4, w, h);
    }
    
    
    @Override
    public void tick(LinkedList<GameObject> objects) {
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.shop, null, (int) x, (int) y);
        
        g.setColor(Color.red);
        g.draw(getBounds());
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, w, h);
    }

    public void checkCollision(LinkedList<GameObject> objects) {
        Player p = Player.getInstance();
        if (getBounds().intersects(p.getBounds())) {
            HUD.openShop = true;
        }
        else {
            HUD.openShop = false;
            HUD.shopPosition = 1;
        }
    }
}
