package objects;

import core.Assets;
import core.GameObject;
import core.GameObject;
import core.ObjectId;
import main.Game;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Christian
 */
public class Button extends GameObject {
    String text;
    BufferedImage normal, hover;
    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, w, h);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        Point mPos = Game.getInstance().getMousePosition();
        
        if(mPos != null){
            if(getBounds().contains(mPos)){
                setToHover();
            }
            else setToNormal();
        }
    }
    
    public enum State{
        NORMAL, HOVER
    }
    public State currentState = State.NORMAL;
    
    public Button(float x, float y, String text) {
        super(ObjectId.BUTTON, x, y, 190, 49);
        this.text = text;
    }
    
    public Button(float x, float y, int w, int h, BufferedImage normal, BufferedImage hover) {
        super(ObjectId.BUTTON,x, y, w, h);
        this.normal = normal;
        this.hover = hover;
    }

    public boolean isNormal(){
        return currentState == State.NORMAL;
    }
    
    public boolean isHover(){
        return currentState == State.HOVER;
    }
    
    public void setToNormal(){
        currentState = State.NORMAL;
    }
    
    public void setToHover(){
        currentState = State.HOVER;
    }
    
//    @Override
//    public void tick(List<GameObject> objects) {

//    }

    @Override
    public void render(Graphics2D g2d) {
//        int fixedX = (w/2) - ((text.length() * 15) / 2);
//        int fixedY = (int) y+30;
        
        if(isHover()){
            g2d.drawImage(hover, (int) x, (int) y, null);
        }
        else g2d.drawImage(normal, (int) x, (int) y, null);
        
//        g2d.setColor(Color.white);
//        g2d.setFont(new Font(Font.MONOSPACED, Font.BOLD, 25));
//        g2d.drawString(text, x+fixedX, fixedY);
    }
    
}
