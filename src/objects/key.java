/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;

/**
 *
 * @author Ucha
 */
public class key extends GameObject{
    
    private int width = 45;
    private int height = 45;
    
    public key(ObjectId id, float x, float y) {
        super(id, x, y);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(Assets.key, (int) getX(), (int) getY(), (int) width, (int) height, null);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.red);
        g2d.draw(getBounds());
        
        g2d.setColor(Color.blue);
        g2d.draw(getBoundsRight());
        g2d.draw(getBoundsLeft());
    }

   @Override
    public Rectangle getBounds() {
        return new Rectangle((int) (x +5) + width/4, (int)y + height/4, (int) width/3, (int)height/2);
    }
    
    public Rectangle getBoundsLeft() {
        return new Rectangle((int) (x +5) + (width/4), (int)y + width/4, (int) 5, (int)height/2);
    }

    public Rectangle getBoundsRight() {
        return new Rectangle((int)x + (width*3/4) - 10, (int)y + width/4, (int) 5, (int)height/2);
    }
    
    public synchronized void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);
            if (tObject.getId() == ObjectId.Player) {
                Player p = (Player) tObject;
                if(this.getBounds().intersects(p.getBounds())){ 
                    p.setKey(true);
                    Game.handler.removeObject(this);
                }
            }
        }
    }
    
}
