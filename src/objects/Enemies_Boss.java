/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Animation;
import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;


/**
 *
 * @author Ucha
 */
public class Enemies_Boss extends GameObject {
    
    public static final float eWIDTH = 32, eHEIGHT = 40;
    public static final float GRAVITY = 0.5f;
    public static final int MAX_GRAVITY = 10;
    public static final int SHOOT_DELAY = 300;
    public static int hitTime=0;
    public static final float moveSpeed = 6.0f;
    public static final float jumpValue = 12.0f;
    public static int shootTime = 0;
    public static int LIFE=10;
    private float tempX ;
    public static int jumpTime = 0;
    
//    private static int stuckPoint = 0;
    private static int damage = 1;
    Animation walkAnim;
    
    public Enemies_Boss(ObjectId id, float x, float y) {
        super(id, x, y);
        this.id=id;
        this.tempX = x;
        velX =moveSpeed/10;
        walkAnim = new Animation(5, Assets.boss_walk);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        walkAnim.runAnimation();
        y += velY;
        x += velX;
        hitTime -=1;
        jumpTime-=1;
        shootTime  -=1;
        if (falling || jumping) {
            velY += GRAVITY;
            
            if (velY > MAX_GRAVITY) {
                velY = MAX_GRAVITY;
            }
        }
        checkCollision(objects);
        
        if(Math.abs(x-this.tempX) >=100 ){
            velX = velX*-1;
//            System.out.println("kena >=100 vel X = "+ velX);
        }else if(Math.abs(x-this.tempX)<0){
            velX=1;
//            System.out.println("kena <0");
        }
        
        
        
    }

    @Override
    public void render(Graphics2D g) {
        if(velX<0){
            walkAnim.drawAnimation(g, (int) x, (int) y,(int)eWIDTH, (int)eHEIGHT, 0);
        }else{
            walkAnim.drawAnimation(g, (int) x, (int) y,(int)eWIDTH, (int)eHEIGHT, 1);
        }
        
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.red);
        g2d.draw(getBounds());
        
        g2d.setColor(Color.blue);
        g2d.draw(getBoundsRight());
        g2d.draw(getBoundsLeft());
    }

    public void fire(int dir) {
            
            Enemy_Bullet bullet = new Enemy_Bullet(ObjectId.Enemy_Bullet, this.getX(), this.getY(),dir, id);
            Game.handler.addObject(bullet);
            this.shootTime = this.SHOOT_DELAY;
        
           
        
    }
    
    @Override
    
     // Bottom Bound
    public Rectangle getBounds() {
        return new Rectangle((int)(x+((eWIDTH/4))), (int)(y+eHEIGHT/2), (int)(eWIDTH/2), (int)eHEIGHT/2);
    }
    
    public Rectangle getBoundsTop() {
        return new Rectangle((int)(x+((eWIDTH/4))), (int)y, (int)(eWIDTH/2), (int)eHEIGHT/2);
    }
    
    public Rectangle getBoundsRight() {
        return new Rectangle((int)(x+((eWIDTH/5)*4)), (int)(y+((int)(eHEIGHT/6)/2)), (int)eWIDTH/5, (int)(eHEIGHT-(eHEIGHT/3)));
    }
    
    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x, (int)(y+((int)(eHEIGHT/6)/2)), (int)eWIDTH/5, (int)(eHEIGHT-(eHEIGHT/3)));
    }
    
    public Rectangle getBoundsBottom() {
        return new Rectangle((int)(x+((eWIDTH/4))), (int)y + (int)eHEIGHT/2, (int)(eWIDTH/2), (int)eHEIGHT/2);
    }
    
    public void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);
            
            if (tObject.getId() == ObjectId.TYPE_GROUND) {
                // When Hit at Top we need to Reposition Y and set VelY to 0
                if (getBoundsTop().intersects(tObject.getBounds())) {
                    y = tObject.getY() + tObject.getH();
                    velY = 0;
                }
                
                // When Hit at Bottom we need to Reposition Y, set VelY to 0
                // Also reset the jumping and falling
                // but once after the hit, we set it to be falling
                if (getBounds().intersects(tObject.getBounds())) {
                    y = tObject.getY() - eHEIGHT;
                    velY = 0;
                    falling = false;
                    jumping = false;
                } else
                    falling = true;
                
                // When Hit at Left we need to Reposition X
                if (getBoundsRight().intersects(tObject.getBounds())) {
                    x = tObject.getX() - eWIDTH;
                    velX = velX*-1;
                }
                
                // When Hit at Right we also need to Reposition X
                if (getBoundsLeft().intersects(tObject.getBounds())) {
                    x = tObject.getX() + tObject.getW();
                    velX = 1;
                }
                
            }
            else if ( tObject.getId() == ObjectId.Bullet) {
                if(Math.abs(tObject.getX()-this.x)<=100 && jumpTime<=0){
                    this.velY = -jumpValue;
                    jumpTime = 400;
                }
                if(this.getBoundsLeft().intersects(tObject.getBounds())
                        || this.getBoundsLeft().intersects(tObject.getBounds())) {
                    
                    if(this.LIFE<=0){
                        key a = new key(id, this.x, this.y);
                        Game.handler.removeObject(this);
                        
                        Game.handler.addObject(a);
                    }else{
                        if(hitTime<=0){
                            hitTime=100;
                            this.LIFE -= Player.ATTACK;
                        }
                    }
                 
                    Game.handler.removeObject(tObject);
                }
            }else if (tObject.getId() == ObjectId.Player) {
                Player p = (Player) tObject;
                if(getBounds().intersects(p.getBounds())){
                    if(hitTime <= 0){
                        hitTime = 300;
                        Player.LIFE -= this.damage;
                        System.out.println("Life = "+Player.LIFE);
                    }
                }else if(Math.abs(p.getX()-this.x) <=300){
                    int dir=1;
                    if(p.getX()-this.x<0){
                        dir=0;
                    }
                    
                   if(shootTime<=0 && Player.LIFE>=0){
                       if(Math.abs(p.getY()-this.y)<=30){
                           this.fire(dir);
                       }
                       
                   } 
                }
            }
        }
    }
}
