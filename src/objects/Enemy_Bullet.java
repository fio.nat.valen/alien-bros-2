/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;

/**
 *
 * @author tjuatja
 */
public class Enemy_Bullet extends GameObject{
    private int direction;
    private ObjectId owner;
    private float velX = 4f;
    private float speed = 1.2f;
    private int width = 45;
    private int height = 45;
    private int bulletTime = 300;
    private static int damage = 1;

    public Enemy_Bullet(ObjectId id, float x, float y, int direction, ObjectId o) {
        super(id, x, y);
        this.owner = o;
        this.direction = direction;
        if(this.direction == 0) {
            velX*= -1;
        }
    }

    // direction => {0 = left, 1 = right}
   

    @Override
    public void tick(LinkedList<GameObject> objects) {
        x+=velX;
        bulletTime--;
        
        if(bulletTime<=0) {
            synchronized (objects) {
                Game.handler.removeObject(this);
            }
        }
        
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {
        if(owner == ObjectId.Boss){
            if(direction == 0) {
            g.drawImage(Assets.boss_bullet, (int) getX(), (int) getY(), (int) width, (int) height, null);
            }
            else {
                g.drawImage(Assets.boss_bullet, (int) getX() + width, (int) getY(), (int) -width, (int) height, null);
            }
        }else{
            if(direction == 0) {
            g.drawImage(Assets.ghost_bullet, (int) getX(), (int) getY(), (int) width, (int) height, null);
            }
            else {
                g.drawImage(Assets.ghost_bullet, (int) getX() + width, (int) getY(), (int) -width, (int) height, null);
            }
        }
        
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.red);
        g2d.draw(getBounds());
        
        g2d.setColor(Color.blue);
        g2d.draw(getBoundsRight());
        g2d.draw(getBoundsLeft());
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) (x +5) + width/4, (int)y + height/4, (int) width/3, (int)height/2);
    }
    
    public Rectangle getBoundsLeft() {
        return new Rectangle((int) (x +5) + (width/4), (int)y + width/4, (int) 5, (int)height/2);
    }

    public Rectangle getBoundsRight() {
        return new Rectangle((int)x + (width*3/4) - 10, (int)y + width/4, (int) 5, (int)height/2);
    }
    
    public static int getDamage() {
        return damage;
    }
    public synchronized void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);
            Rectangle bound = direction==0 ? (getBoundsLeft()) : (getBoundsRight());
            
            if (bound.intersects(tObject.getBounds())) {
                if (tObject.getId() == ObjectId.TYPE_GROUND) {
                    synchronized (objects) {
                        Game.handler.removeObject(this);
                    }
                }
            }else if (tObject.getId() == ObjectId.Player) {
                Player p = (Player) tObject;
                if(getBounds().intersects(p.getBounds())){
                    Player.LIFE -= this.damage;
                    Game.handler.removeObject(this);
                }
            }
        }
    }
    
}
