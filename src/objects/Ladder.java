/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import state.State;

/**
 *
 * @author Frans
 */
public class Ladder extends GameObject {

    public Ladder(ObjectId id, float x, float y, int w, int h) {
        super(id, x, y, w, h);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {

        g.setColor(Color.yellow);
        g.draw(getBounds());
        g.setColor(Color.green);
        g.draw(getBoundsTop());
        g.setColor(Color.green);
        g.draw(getBoundsBottom());
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, w, h);
    }

    public Rectangle getBoundsTop() {
        return new Rectangle((int) x, (int) y, w, 10);
    }

    public Rectangle getBoundsBottom() {
        return new Rectangle((int) x, (int) y + h - 1, w, 1);
    }

    public void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);
            
            
            if (tObject.getId() == ObjectId.Boss || tObject.getId() == ObjectId.Enemy) {
                if(tObject.getBounds().intersects(getBoundsTop())) {
                    tObject.setY(getY() - 48);
                    velY = 0;
                }
            }
        }

    }

}
