/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;

/**
 *
 * @author Frans
 */
public class Spike extends GameObject{

    public Spike(ObjectId id, float x, float y, int w, int h) {
        super(id, x, y, w, h);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
        checkCollision(objects);
    }

    @Override
    public void render(Graphics2D g) {
        
        g.setColor(Color.red);
        g.draw(getBounds());
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y + h/2, w, h/2);
    }
    
    public void checkCollision(LinkedList<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            GameObject tObject = objects.get(i);

            if (tObject.getId() == ObjectId.Player) {
                Player p = (Player) tObject;
                if(getBounds().intersects(p.getBounds()) && Player.LIFE>0){
                    Player.LIFE = 0;
                }
            }
        }
    }
    
}
