/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.awt.event.KeyEvent;
import objects.Player;

/**
 *
 * @author Frans
 */
public class DuckingState implements State {

    @Override
    public void handleAction(Player player, KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            player.state = State.ducking;
        }
    }

    @Override
    public void handleKeyRelease(Player player, KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            player.state = State.standing;
        }
    }

    @Override
    public void update(Player hero) {
        
    }


    
}
