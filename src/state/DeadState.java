/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.awt.event.KeyEvent;
import objects.Player;

/**
 *
 * @author Frans
 */
public class DeadState implements State{

    @Override
    public void handleAction(Player player, KeyEvent e) {
        player.setVelX(0);
        player.setVelY(0);
    }

    @Override
    public void handleKeyRelease(Player player, KeyEvent e) {
        
    }

    @Override
    public void update(Player player) {
        
    }
    
}
