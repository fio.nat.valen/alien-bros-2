/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.awt.event.KeyEvent;
import objects.Player;


/**
 *
 * @author Frans
 */
public class StandingState implements State{
    
    @Override
    public void handleAction(Player player, KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE)
        {
            player.state = State.jumping;
            player.state.handleAction(player, e);
        }
        else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
            
            if (player.onTopLadder) {
                player.state = State.climbing;
                player.state.handleAction(player, e);
            }
            else {
//                player.state = State.ducking;
            }
            
        }
        else if(e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            player.state = State.moving;
            player.state.handleAction(player, e);
        }
        else if(e.getKeyCode() == KeyEvent.VK_Z) {
            player.fire();
        }
        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            if(player.onBottomLadder) {
                player.state = State.climbing;
                player.state.handleAction(player, e);
            }
        }
    }

    @Override
    public void handleKeyRelease(Player player, KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            player.setVelX(0);
        }
    }
    
    @Override
    public void update(Player player) {
        
    }
    
}
