/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.awt.event.KeyEvent;
import objects.Player;

/**
 *
 * @author Frans
 */
public interface State {
    void handleAction(Player hero, KeyEvent e);
    void handleKeyRelease(Player hero, KeyEvent e);
    void update(Player hero);
    
    static DuckingState ducking = new DuckingState();
    static JumpingState jumping = new JumpingState();
    static StandingState standing = new StandingState();
    static MovingState moving = new MovingState();
    static ClimbingState climbing = new ClimbingState();
    static DeadState dead = new DeadState();
}