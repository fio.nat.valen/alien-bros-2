package main;

import core.Assets;
import core.Camera;
import objects.Player;
import core.Handler;
import core.KeyHandler;
import core.MouseHandler;
import core.ObjectId;
import core.Sound;
import core.Window;
import core.XmlLoader;
import java.applet.AudioClip;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import objects.Coin;
import objects.DrawnObject;
import objects.UndrawnObject;
import objects.Enemies;
import objects.Enemies_Boss;
import objects.Enemy_Barnacle;
import objects.ExitPoint;
import objects.Ladder;
import objects.Shop;
import objects.Spike;
import state.State;

public class Game extends Canvas implements Runnable {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final int FPS = 60;
    
    private boolean running = false;
    private Thread thread;
    public static Handler handler;
    public HUD hud;
    public static KeyHandler keyHandler;
    public static Camera camera;
    public static Player player;
    private Enemies[] ArrayOfEnemies = new Enemies[10];
    public static boolean addKey = true;
    private MouseHandler mouseHandler;
    
    static Game game;
    public Menu menu;
    
    public Sound soundMain;
    public Sound soundLevel;
    
    public enum GameState{
        MAIN_MENU, LEVEL_CANDY, LEVEL_ICE, LEVEL_CHOCO, FINISH;
    }
    public static GameState currentState = GameState.MAIN_MENU;

    public synchronized void start() {
        if (running)
            return;
        
        thread = new Thread(this);
        running = true;
        thread.start();
    }
    
    public void init() {
        
        handler = new Handler();
        camera = new Camera(0, 0);
        hud = new HUD();
        player = Player.getInstance();
        loadLevel("map_candy.tmx", player);
        addKeyListener(hud);
        menu = new Menu();
        
        mouseHandler = new MouseHandler();
        addMouseListener(mouseHandler);
        soundMain = new Sound("introduction.wav");
        soundLevel = new Sound("music_box.wav");
    }
    
    public static void loadLevel(String url, Player p){
        Handler temp = new Handler();

        XmlLoader xmlLoader = new XmlLoader(url);
        ArrayList<UndrawnObject> uList =xmlLoader.getUdObjList();
        ArrayList<Coin> cList =xmlLoader.getCoinList();
        ArrayList<Spike> sList =xmlLoader.getSpikeList();
        ArrayList<Ladder> lList =xmlLoader.getLadderList();
        ArrayList<ExitPoint> eList =xmlLoader.getExitList();
        Shop shop = xmlLoader.getShop();
        int[] start = xmlLoader.getStart();
        
        temp.addObject(shop);
        
        for (Coin c : cList) {
            temp.addObject(c);
        }
        
        for (Spike s : sList) {
            temp.addObject(s);
        }
        
        for (Ladder l : lList) {
            temp.addObject(l);
        }
        
        for (ExitPoint e : eList) {
            temp.addObject(e);
        }
        
        for (UndrawnObject undrawnObject : uList) {
            temp.addObject(undrawnObject);
        }
        
        for (Enemies e : xmlLoader.getEnemiesList()) {
            temp.addObject(e);
        }
        
        for (Enemy_Barnacle e : xmlLoader.getEnemies2List()) {
            temp.addObject(e);
        }
        
        temp.addObject(xmlLoader.getBoss());
        
        p.setX(start[0]);
        p.setY(start[1]);
        
        handler = temp;
        handler.addObject(p);
        keyHandler = new KeyHandler(handler, p);
        
        addKey = true;
        
    }
    public static Game getInstance(){
        return game;
    }
    public void playGame() {
        currentState = GameState.LEVEL_CANDY;
        Player.GOLD = 300000;
        Player.LIFE = 3;
        Player.PLAYER_TYPE = 0;
        Player.getInstance().state = State.standing;
        removeMouseListener(mouseHandler);
        loadLevel("map_candy.tmx", Player.getInstance());
    }
    @Override
    public void run() {
        init();
        long lastTime = System.nanoTime();
        double amountOfTicks = FPS;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                try {
                    tick();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
                updates++;
                delta--;
            }
            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frames = 0;
                updates = 0;
            }
        }
    }
    
    private void tick() throws InterruptedException {
        switch(currentState){
            case MAIN_MENU: 
                menu.tick();
                soundMain.play();
                break;
            default:{
                soundLevel.loop();
                if(addKey) {
                    this.addKeyListener(keyHandler);
                    addKey = false;
                }

                handler.tick();
                keyHandler.tick();

                for (int i = 0; i < handler.objects.size(); i++) {

                    if(handler.objects.get(i) != null) {
                        if (handler.objects.get(i).getId() == ObjectId.Player) {
                            camera.tick(handler.objects.get(i));
                        }
                    }
                    else {
                        System.out.println("Size nya : " + handler.objects.size());
                    }
                }
                hud.tick();
            }
        }
        
    }
    
    private void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            bs = getBufferStrategy();
        }
        
        Graphics g = bs.getDrawGraphics();
        Graphics g2d = (Graphics2D) g;
        
        // Draw Game Object
        
        switch(currentState){
            case MAIN_MENU:
                removeMouseListener(mouseHandler);
                addMouseListener(mouseHandler);
                menu.render((Graphics2D)g);
                
                break;
            case LEVEL_CANDY:
                g.setColor(Color.black);
                g.fillRect(0, 0, WIDTH, HEIGHT);
                g2d.translate((int)camera.getX(), (int)camera.getY());
                
                g.drawImage(Assets.mapCandy, 0, 0, this);

                handler.render((Graphics2D) g);
                g2d.translate((int)-camera.getX(), (int)-camera.getY());
                hud.render((Graphics2D) g);
                
                break;
            case LEVEL_CHOCO:
                g.setColor(Color.black);
                g.fillRect(0, 0, WIDTH, HEIGHT);
                g2d.translate((int)camera.getX(), (int)camera.getY());
                
                g.drawImage(Assets.mapChoco, 0, 0, this);
                
                handler.render((Graphics2D) g);
                g2d.translate((int)-camera.getX(), (int)-camera.getY());
                hud.render((Graphics2D) g);
                break;
            case LEVEL_ICE:
                g.setColor(Color.black);
                g.fillRect(0, 0, WIDTH, HEIGHT);
                g2d.translate((int)camera.getX(), (int)camera.getY());
                
                g.drawImage(Assets.mapIce, 0, 0, this);
                
                handler.render((Graphics2D) g);
                g2d.translate((int)-camera.getX(), (int)-camera.getY());
                hud.render((Graphics2D) g);
                break;
            case FINISH:
                hud.render((Graphics2D) g);
                
        }
        
        g.dispose();
        bs.show();
    }
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Alien Run");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMaximumSize(new Dimension(WIDTH, HEIGHT));
        
        game = new Game();
        game.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        game.requestFocus();
        game.setFocusable(true);
        
        frame.add(game);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        game.start();
    }
    
}
