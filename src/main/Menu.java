package main;

import core.Assets;
import objects.Button;
import core.ImageLoader;
import core.Sound;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Christian
 */
public class Menu {
    BufferedImage background;
    public Button btnPlay;
    public Button btnExit;
    
    public Menu(){
        background = Assets.menu_bg;
        btnPlay = new Button(300, 350,200, 200,Assets.menu_play, Assets.menu_play_hover);
        btnExit = new Button(20, 20,50, 50,Assets.menu_exit, Assets.menu_exit_hover);
    }
    
    public void tick(){
        btnPlay.tick(null);
        btnExit.tick(null);
    }
    
    public void render(Graphics2D g2d){
        
        g2d.drawImage(background, 0, 0, null);
        btnPlay.render(g2d);
        btnExit.render(g2d);
        
    }
}
