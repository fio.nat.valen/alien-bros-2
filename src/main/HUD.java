package main;

import core.Assets;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import objects.Player;
import state.State;

public class HUD implements KeyListener{
    BufferedImage heart;
    BufferedImage heart_empty;
    BufferedImage coin;
    BufferedImage panel;
    BufferedImage selector;
    Font customFont;
    
    boolean restart = true;
    public static boolean openShop = false;
    
    public static int shopPosition = 1;
    
    public HUD() {
        heart = Assets.hud_heart;
        heart_empty = Assets.hud_heart_empty;
        coin = Assets.hud_coin;
        panel = Assets.hud_panel;
        selector = Assets.hud_selector;
        customFont = Assets.customFont;
    }
    
    public void tick() {
        
    }
    
    public void render(Graphics2D g){
        
        String coins = String.valueOf(Player.GOLD);
        String[] coinSprite = coins.split("");
        String[] gameover = "GAMEOVER".split("");
        String[] win = "YOUWIN".split("");
        
        // draw coin
        g.drawImage(resize(coin, 45, 45), 0, 5, null);
        
        for(int i=0; i<coinSprite.length; i++) {
            BufferedImage number = getImageString(coinSprite[i]);
            
            g.drawImage(resize(number, 50, 50), 0 + (40 * (i + 1)), 0, null);
        }
        
        g.drawImage(resize(Assets.hud_attack, 45, 45), 700, 45, null);
        
        if(Player.ATTACK==1) {
            g.drawImage(resize(Assets.hud_1, 60, 60), 730, 35, null);
        }
        else {
            g.drawImage(resize(Assets.hud_2, 60, 60), 730, 35, null);            
        }
        
        // draw heart
        for (int i = 4; i > 0; i--) {
            if(i <= Player.LIFE ) {
                g.drawImage(resize(heart, 50, 50), 580 + (40 * (4-i + 1)), 0, null);
            }
            else {
                if(Player.PLAYER_TYPE==2 && i==4) {
                    g.drawImage(resize(heart_empty, 50, 50), 580 + (40 * (4 - i + 1)), 0, null);
                }
                else if(i<4) {
                    g.drawImage(resize(heart_empty, 50, 50), 580 + (40 * (4-i + 1)), 0, null);
                }
            }
        }
        
        
        // game over screen
        if (Player.LIFE <= 0) {
            g.drawImage(resize(panel, 450, 350), Game.WIDTH/2-225, Game.HEIGHT/2-175, null);
            
            for(int i=0; i<gameover.length; i++) {
                if(i >= gameover.length/2) {
                    g.drawImage(getImageString(gameover[i]), 195 + (70 * (i - gameover.length/2 + 1)),
                            (Game.HEIGHT / 2) - 60, null);
                }
                else {
                    g.drawImage(getImageString(gameover[i]), 195 + (70 * (i + 1)),
                            (Game.HEIGHT / 2) - 135, null);
                }
            }
            
            if(restart) {
                g.drawImage(resize(selector, 18, 12), 270, (Game.HEIGHT - 252), null);
            }
            else {
                g.drawImage(resize(selector, 18, 12), 270, (Game.HEIGHT - 222), null);
            }
            g.setColor(Color.GRAY);
            g.setFont(customFont);
            g.drawString("TRY AGAIN ?", 270, (Game.HEIGHT - 270));
            g.drawString("YES", 295, (Game.HEIGHT - 240));
            g.drawString("NO", 295, (Game.HEIGHT - 210));
        }
        else if(Game.currentState== Game.GameState.FINISH) {        // END GAME
            // draw panel
            g.drawImage(resize(panel, 450, 350), Game.WIDTH/2-225, Game.HEIGHT/2-175, null);
            
            for (int i = 0; i < win.length; i++) {
                if (i >= win.length / 2) {
                    g.drawImage(getImageString(win[i]), 230 + (70 * (i - win.length / 2 + 1)),
                            (Game.HEIGHT / 2) - 60, null);
                } else {
                    g.drawImage(getImageString(win[i]), 230 + (70 * (i + 1)),
                            (Game.HEIGHT / 2) - 135, null);
                }
            }
            
            g.drawImage(resize(selector, 18, 12), 270, (Game.HEIGHT - 262), null);
            g.setColor(Color.GRAY);
            g.setFont(customFont);
            g.drawString("BACK TO MAIN MENU", 295, (Game.HEIGHT - 250));
        }
        
        if(openShop) {
            switch(shopPosition) {
                case 1 : {
                    g.drawImage(Assets.shop_1, Game.WIDTH / 2 - 225, Game.HEIGHT / 2 - 175, null);
                }break;
                case 2 : {
                    g.drawImage(Assets.shop_2, Game.WIDTH / 2 - 225, Game.HEIGHT / 2 - 175, null);
                }break;
                case 3 : {
                    g.drawImage(Assets.shop_3, Game.WIDTH / 2 - 225, Game.HEIGHT / 2 - 175, null);
                }break;
            }
        }
        
    }
    
    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }
    
    public static BufferedImage getImageString(String number) {
        BufferedImage temp = null;
        
        switch(number) {
            case "0": {
                temp = Assets.hud_0;
            }break;
            case "1": {
                temp = Assets.hud_1;
            }break;
            case "2": {
                temp = Assets.hud_2;
            }break;
            case "3": {
                temp = Assets.hud_3;
            }break;
            case "4": {
                temp = Assets.hud_4;
            }
            break;
            case "5": {
                temp = Assets.hud_5;
            }
            break;
            case "6": {
                temp = Assets.hud_6;
            }
            break;
            case "7": {
                temp = Assets.hud_7;
            }
            break;
            case "8": {
                temp = Assets.hud_8;
            }
            break;
            case "9": {
                temp = Assets.hud_9;
            }
            break;
            case "G": {
                temp = Assets.hud_G;
            }
            break;
            case "A": {
                temp = Assets.hud_A;
            }
            break;
            case "M": {
                temp = Assets.hud_M;
            }
            break;
            case "E": {
                temp = Assets.hud_E;
            }
            break;
            case "O": {
                temp = Assets.hud_O;
            }
            break;
            case "V": {
                temp = Assets.hud_V;
            }
            break;
            case "R": {
                temp = Assets.hud_R;
            }
            break;
            case "Y": {
                temp = Assets.hud_Y;
            }
            break;
            case "U": {
                temp = Assets.hud_U;
            }
            break;
            case "W": {
                temp = Assets.hud_W;
            }
            break;
            case "I": {
                temp = Assets.hud_I;
            }
            break;
            case "N": {
                temp = Assets.hud_N;
            }
            break;
        }
        
        return temp;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(Player.LIFE <= 0) {
            if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
                restart = !restart;
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                if (restart) {
                    Player.GOLD = 0;
                    Player.LIFE = 3;
                    Player.PLAYER_TYPE = 0;
                    Player.getInstance().state = State.standing;
                    switch (Game.currentState) {
                        case LEVEL_CANDY:
                            Game.loadLevel("map_candy.tmx", Player.getInstance());
                            break;
                        case LEVEL_CHOCO:
                            Game.loadLevel("map_choco.tmx", Player.getInstance());
                            break;
                        case LEVEL_ICE:
                            Game.loadLevel("map_ice.tmx", Player.getInstance());
                            break;
                    }
                } else {
                    Game.currentState = Game.GameState.MAIN_MENU;
                }
            }
        }
        
        if(openShop) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                switch (shopPosition) {
                    case 1: {
                        if(Player.GOLD>=1000) {
                            if(Player.PLAYER_TYPE==2) {
                                Player.LIFE = 4;                                
                            }
                            else {
                                Player.LIFE = 3;                                
                            }
                            Player.GOLD -= 1000;
                        }
                    }
                    break;
                    case 2: {
                        if(Player.GOLD>=3000) {
                            Player.GOLD -= 3000;
                            Player.ATTACK = 2;
                            Player.PLAYER_TYPE = 1;
                        }
                    }
                    break;
                    case 3: {
                        if(Player.GOLD>=3000) {
                            Player.ATTACK = 1;
                            Player.GOLD -= 3000;
                            Player.PLAYER_TYPE = 2;
                            Player.LIFE = 4;
                        }
                    }
                    break;
                }
            }
            else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                shopPosition++;
            }
            else if (e.getKeyCode() == KeyEvent.VK_UP) {
                shopPosition--;
            }
            
            if(shopPosition<=0) shopPosition=3;
            if(shopPosition>3) shopPosition=1;
        }

        if (Game.currentState == Game.GameState.FINISH) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                Game.currentState = Game.GameState.MAIN_MENU;
            }
        }
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
}
