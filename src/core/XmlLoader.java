/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import objects.Coin;
import objects.DrawnObject;
import objects.Enemies;
import objects.Enemies_Boss;
import objects.Enemy_Barnacle;
import objects.ExitPoint;
import objects.Ladder;
import objects.Shop;
import objects.Spike;
import objects.UndrawnObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Lenovo
 */
public class XmlLoader {

    String url;
    ArrayList<DrawnObject> dObjList = new ArrayList<>();
    ArrayList<Coin> coinList = new ArrayList<>();
    ArrayList<Spike> spikeList = new ArrayList<>();
    ArrayList<UndrawnObject> udObjList = new ArrayList<>();
    ArrayList<Ladder> ladderList = new ArrayList<>();
    ArrayList<ExitPoint> exitList = new ArrayList<>();
    ArrayList<Enemies> enemiesList = new ArrayList<>();
    ArrayList<Enemy_Barnacle> enemies2List = new ArrayList<>();
    Enemies_Boss boss;
    
    
    int[] start = new int[2];
    Shop shop;

    public XmlLoader(String url) {
        try {
            File inputFile = new File("src/assets/tiles/" + url);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

//         System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("objectgroup");
//         System.out.println("----------------------------");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
//            System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
//               System.out.println("objectgroup name : " + eElement.getAttribute("name"));
                    NodeList childNodeList = eElement.getChildNodes();

                    for (int i = 0; i < childNodeList.getLength(); i++) {
                        Node childNode = childNodeList.item(i);
                        if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element childElement = (Element) childNode;
                            float x = Float.parseFloat(childElement.getAttribute("x"));
                            float y = Float.parseFloat(childElement.getAttribute("y"));
                            int w = Integer.parseInt(childElement.getAttribute("width"));
                            int h = Integer.parseInt(childElement.getAttribute("height"));

                            if (eElement.getAttribute("name").equalsIgnoreCase("coins")) {
//                            DrawnObject doObj = new DrawnObject(ObjectId.TYPE_COINS, x, y, w, h);
                                Coin doObj = new Coin(ObjectId.TYPE_COINS, x, y, w, h);
                                coinList.add(doObj);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("obstacles")) {
                                Spike spike = new Spike(ObjectId.Spike, x, y, w, h);
                                spikeList.add(spike);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("ladder")) {
                                Ladder ladder = new Ladder(ObjectId.Ladder, x, y, w, h);
                                ladderList.add(ladder);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("exit")) {
                                ExitPoint exit = new ExitPoint(ObjectId.TYPE_EXIT, x, y, w, h);
                                exitList.add(exit);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("enemy_1")) {
                                Enemies e = new Enemies(ObjectId.Enemy, x, y);
                                enemiesList.add(e);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("enemy_2")) {
                                Enemy_Barnacle b= new Enemy_Barnacle(ObjectId.Enemy, x, y);
                                enemies2List.add(b);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("boss")) {
                                boss = new Enemies_Boss(ObjectId.Boss, x, y);
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("shop")) {
                                shop = new Shop(ObjectId.Shop, x, y, w, h);
                                System.out.println(shop.getX() + " X");
                                System.out.println(shop.getY() + " Y");
                            } else if (eElement.getAttribute("name").equalsIgnoreCase("start")) {
                                start[0] = (int) x;
                                start[1] = (int) y;
                            } else {
                                UndrawnObject udo = new UndrawnObject(ObjectId.TYPE_GROUND, x, y, w, h);
                                if (eElement.getAttribute("name").equalsIgnoreCase("ground")) {
                                    udo = new UndrawnObject(ObjectId.TYPE_GROUND, x, y, w, h);
                                    udObjList.add(udo);
                                }
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<DrawnObject> getdObjList() {
        return dObjList;
    }

    public void setdObjList(ArrayList<DrawnObject> dObjList) {
        this.dObjList = dObjList;
    }

    public ArrayList<UndrawnObject> getUdObjList() {
        return udObjList;
    }

    public void setUdObjList(ArrayList<UndrawnObject> udObjList) {
        this.udObjList = udObjList;
    }

    public ArrayList<Coin> getCoinList() {
        return coinList;
    }

    public void setCoinList(ArrayList<Coin> coinList) {
        this.coinList = coinList;
    }

    public ArrayList<Spike> getSpikeList() {
        return spikeList;
    }

    public void setSpikeList(ArrayList<Spike> spikeList) {
        this.spikeList = spikeList;
    }

    public ArrayList<Ladder> getLadderList() {
        return ladderList;
    }

    public void setLadderList(ArrayList<Ladder> ladderList) {
        this.ladderList = ladderList;
    }

    public ArrayList<ExitPoint> getExitList() {
        return exitList;
    }

    public void setExitList(ArrayList<ExitPoint> exitList) {
        this.exitList = exitList;
    }

    public int[] getStart() {
        return start;
    }

    public void setStart(int[] start) {
        this.start = start;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public ArrayList<Enemies> getEnemiesList() {
        return enemiesList;
    }

    public void setEnemiesList(ArrayList<Enemies> enemiesList) {
        this.enemiesList = enemiesList;
    }

    public ArrayList<Enemy_Barnacle> getEnemies2List() {
        return enemies2List;
    }

    public void setEnemies2List(ArrayList<Enemy_Barnacle> enemies2List) {
        this.enemies2List = enemies2List;
    }

    public Enemies_Boss getBoss() {
        return boss;
    }

    public void setBoss(Enemies_Boss boss) {
        this.boss = boss;
    }

    
    
}
