package core;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Assets {
    static ImageLoader loader = new ImageLoader();
    
    public final static BufferedImage idle_1 = loader.load("player/1/idle.png");    
    public final static BufferedImage idle_2 = loader.load("player/2/idle.png");    
    public final static BufferedImage idle_3 = loader.load("player/3/idle.png");    
    public final static BufferedImage[] player_idle = {idle_1,idle_2,idle_3};
    
    public final static BufferedImage jump_1 = loader.load("player/1/jump.png");
    public final static BufferedImage jump_2 = loader.load("player/2/jump.png");
    public final static BufferedImage jump_3 = loader.load("player/3/jump.png");
    public final static BufferedImage[] player_jump = {jump_1, jump_2, jump_3};
    
    public final static BufferedImage duck_1 = loader.load("player/1/duck.png");
    public final static BufferedImage duck_2 = loader.load("player/2/duck.png");
    public final static BufferedImage duck_3 = loader.load("player/3/duck.png");
    public final static BufferedImage[] player_duck = {duck_1, duck_2, duck_3};
    
    public final static BufferedImage dead_1 = loader.load("player/1/dead.png");
    public final static BufferedImage dead_2 = loader.load("player/2/dead.png");
    public final static BufferedImage dead_3 = loader.load("player/3/dead.png");
    public final static BufferedImage[] player_dead = {dead_1, dead_2, dead_3};
    
    public final static BufferedImage walk1_1 = loader.load("player/1/walk1.png");
    public final static BufferedImage walk1_2 = loader.load("player/1/walk2.png");
    public final static BufferedImage walk2_1 = loader.load("player/2/walk1.png");
    public final static BufferedImage walk2_2 = loader.load("player/2/walk2.png");
    public final static BufferedImage walk3_1 = loader.load("player/3/walk1.png");
    public final static BufferedImage walk3_2 = loader.load("player/3/walk2.png");
    
    
    public final static BufferedImage climb1_1 = loader.load("player/1/climb1.png");
    public final static BufferedImage climb1_2 = loader.load("player/1/climb2.png");
    public final static BufferedImage climb2_1 = loader.load("player/2/climb1.png");
    public final static BufferedImage climb2_2 = loader.load("player/2/climb2.png");
    public final static BufferedImage climb3_1 = loader.load("player/3/climb1.png");
    public final static BufferedImage climb3_2 = loader.load("player/3/climb2.png");
    
    
    public final static BufferedImage[] player_walk_1 = {walk1_1,walk1_2};
    public final static BufferedImage[] player_climb_1 = {climb1_1,climb1_2};
    public final static BufferedImage[] player_walk_2 = {walk2_1,walk2_2};
    public final static BufferedImage[] player_climb_2 = {climb2_1,climb2_2};
    public final static BufferedImage[] player_walk_3 = {walk3_1,walk3_2};
    public final static BufferedImage[] player_climb_3 = {climb3_1,climb3_2};

    public final static BufferedImage[][] player_climb = {player_climb_1, player_climb_2, player_climb_3};
    public final static BufferedImage[][] player_walk = {player_walk_1, player_walk_2, player_walk_3};
        
    
    
    public final static BufferedImage particles_bullet = loader.load("particles/fireball.png");
    public final static BufferedImage mapCandy = loader.load("tiles/map_candy.png");
    public final static BufferedImage mapChoco = loader.load("tiles/map_choco.png");
    public final static BufferedImage mapIce = loader.load("tiles/map_ice.png");
    public final static BufferedImage coins = loader.load("tiles/sheet_items.png").getSubimage(0, 0, 70, 70);
    public final static BufferedImage ghost = loader.load("enemies/ghost.png");
    public final static BufferedImage boss = loader.load("enemies/marmut.png");
    public final static BufferedImage barnacle = loader.load("enemies/barnacle.png");
    public final static BufferedImage barnacle1 = loader.load("enemies/barnacle_bite.png");
    public final static BufferedImage[] barnacle_walk = {barnacle,barnacle1};
    public final static BufferedImage boss_bullet = loader.load("particles/jagung.png");
    public final static BufferedImage bwalk1 = loader.load("enemies/walk1.png");
    public final static BufferedImage bwalk2 = loader.load("enemies/walk2.png");
    public final static BufferedImage bwalk3 = loader.load("enemies/walk3.png");
    public final static BufferedImage bwalk4 = loader.load("enemies/walk4.png");
    public final static BufferedImage[] boss_walk = {bwalk1, bwalk2, bwalk3, bwalk4};
    public final static BufferedImage ghost_bullet = loader.load("particles/ufoBlue.png");
    public final static BufferedImage key = loader.load("enemies/key.png");
    
    public final static BufferedImage hud_heart = loader.load("hud/heart.png");
    public final static BufferedImage hud_attack = loader.load("hud/attack.png");
    public final static BufferedImage hud_heart_empty = loader.load("hud/heart_empty.png");
    public final static BufferedImage hud_panel = loader.load("hud/grey_panel.png");
    public final static BufferedImage hud_coin = loader.resize(loader.load("hud/coin.png"), 60, 60);
    public final static BufferedImage hud_0 = loader.resize(loader.load("hud/hud0.png"), 60, 60);
    public final static BufferedImage hud_1 = loader.resize(loader.load("hud/hud1.png"), 60, 60);
    public final static BufferedImage hud_2 = loader.resize(loader.load("hud/hud2.png"), 60, 60);
    public final static BufferedImage hud_3 = loader.resize(loader.load("hud/hud3.png"), 60, 60);
    public final static BufferedImage hud_4 = loader.resize(loader.load("hud/hud4.png"), 60, 60);
    public final static BufferedImage hud_5 = loader.resize(loader.load("hud/hud5.png"), 60, 60);
    public final static BufferedImage hud_6 = loader.resize(loader.load("hud/hud6.png"), 60, 60);
    public final static BufferedImage hud_7 = loader.resize(loader.load("hud/hud7.png"), 60, 60);
    public final static BufferedImage hud_8 = loader.resize(loader.load("hud/hud8.png"), 60, 60);
    public final static BufferedImage hud_9 = loader.resize(loader.load("hud/hud9.png"), 60, 60);
    
    public final static BufferedImage hud_G = loader.resize(loader.load("hud/letter_G.png"), 60, 60);
    public final static BufferedImage hud_A = loader.resize(loader.load("hud/letter_A.png"), 60, 60);
    public final static BufferedImage hud_M = loader.resize(loader.load("hud/letter_M.png"), 60, 60);
    public final static BufferedImage hud_E = loader.resize(loader.load("hud/letter_E.png"), 60, 60);
    public final static BufferedImage hud_O = loader.resize(loader.load("hud/letter_O.png"), 60, 60);
    public final static BufferedImage hud_V = loader.resize(loader.load("hud/letter_V.png"), 60, 60);
    public final static BufferedImage hud_R = loader.resize(loader.load("hud/letter_R.png"), 60, 60);
    
    public final static BufferedImage hud_Y = loader.resize(loader.load("hud/letter_Y.png"), 60, 60);
    public final static BufferedImage hud_U = loader.resize(loader.load("hud/letter_U.png"), 60, 60);
    public final static BufferedImage hud_W = loader.resize(loader.load("hud/letter_W.png"), 60, 60);
    public final static BufferedImage hud_I = loader.resize(loader.load("hud/letter_I.png"), 60, 60);
    public final static BufferedImage hud_N = loader.resize(loader.load("hud/letter_N.png"), 60, 60);
    
    public final static BufferedImage hud_selector = loader.load("hud/selector.png");
    
    public final static BufferedImage shop_1 = loader.load("hud/select-1.png");
    public final static BufferedImage shop_2 = loader.load("hud/select-2.png");
    public final static BufferedImage shop_3 = loader.load("hud/select-3.png");

    public final static BufferedImage shop = loader.resize(loader.load("hud/sign.png"), 66, 66);
    public final static BufferedImage menu_bg = loader.load("bg.png");
    public final static BufferedImage menu_play = loader.load("play.png");
    public final static BufferedImage menu_exit = loader.load("exit.png");
    public final static BufferedImage menu_play_hover = loader.load("play_hover.png");
    public final static BufferedImage menu_exit_hover = loader.load("exit_hover.png");
    
    public final static Sound balloon_pop = new Sound("balloon_pop.wav");
    public final static Sound introduction = new Sound("introduction.wav");
    public final static Sound pain = new Sound("pain.wav");
    public final static Sound walking = new Sound("walking.wav");
    
    
    public final static Font customFont = getFont();
    
    static Font getFont() {
        try {
            Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("src/assets/font/kenvector_future.ttf")).deriveFont(20f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            //register the font
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("src/assets/font/kenvector_future.ttf")));
        
            return customFont;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FontFormatException e) {
            e.printStackTrace();
        }
        return null;
    }
}
