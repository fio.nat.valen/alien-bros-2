package core;

public enum ObjectId {
    key(),
    Player(),
    Enemy(),
    Boss(),
    Enemy_Bullet(),
    Bullet(),
    TYPE_GROUND(), 
    TYPE_EXIT(), 
    TYPE_LADDER(), 
    Spike(),
    Ladder(),
    TYPE_COINS(),
    Shop(),
    BUTTON();
}
