package core;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import objects.Player;

public class KeyHandler extends KeyAdapter {
    Handler handler;
    Player player;

    public KeyHandler(Handler handler, Player player) {
        this.handler = handler;
        this.player = player;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        handler.removeKey(key);
        player.state.handleKeyRelease(player, e);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        handler.addKey(e.getKeyCode());
        player.state.handleAction(player, e);
    }
    
    public void tick() {
        
    }
}
