/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.HUD;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class ShopTest {
    
    public ShopTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Shop.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Shop instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class Shop.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Shop instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBounds method, of class Shop.
//     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        Shop instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkCollision method, of class Shop.
     */
    @Test
    public void testCheckCollision_haveIntersection() {
        System.out.println("checkCollision");
        boolean expectedResult = true;
        Player player;
        player = Player.getInstance();
        player.setX(5);
        player.setY(5);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        Shop instance = new Shop(ObjectId.Shop, 30,30,15, 15);
        
        instance.checkCollision(objects);
        boolean actualRes = HUD.openShop;
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(expectedResult, actualRes);
    }

    
}
