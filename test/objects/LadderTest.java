/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class LadderTest {
    
    public LadderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Ladder.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = new LinkedList<GameObject>();
//        Ladder instance = new Ladder(ObjectId.Ladder, 0,0,0,0);
//        instance.tick(objects);
//       
//    }
//
//    /**
//     * Test of render method, of class Ladder.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Ladder instance = new Ladder(ObjectId.Ladder, 0,0,0, 0);
//        instance.render(g);
//       
//    }

    /**
     * Test of getBounds method, of class Ladder.
     */
    @Test
    public void testGetBounds() {
        System.out.println("getBounds");
        Ladder instance = new Ladder(ObjectId.Ladder, 0,0,0,0);
        Rectangle expResult = new Rectangle(0,0,0,0);
        Rectangle result = instance.getBounds();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getBoundsTop method, of class Ladder.
     */
    @Test
    public void testGetBoundsTop() {
        System.out.println("getBoundsTop");
        Ladder instance = new Ladder(ObjectId.Ladder, 30,30,70,70);
        Rectangle expResult = new Rectangle(30,30,70,10);
        Rectangle result = instance.getBoundsTop();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getBoundsBottom method, of class Ladder.
     */
    @Test
    public void testGetBoundsBottom() {
        System.out.println("getBoundsBottom");
        Ladder instance = new Ladder(ObjectId.Ladder, 30,30,70,70);
        Rectangle expResult = new Rectangle(30,99,70,1);
        Rectangle result = instance.getBoundsBottom();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of checkCollision method, of class Ladder.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        Ladder instance = new Ladder(ObjectId.Ladder, 30,30,70,70);
        instance.checkCollision(objects);
       
    }
    
}
