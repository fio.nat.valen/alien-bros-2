/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class Enemy_BarnacleTest {
    
    public Enemy_BarnacleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Enemy_Barnacle.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Enemy_Barnacle instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class Enemy_Barnacle.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Enemy_Barnacle instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getBounds method, of class Enemy_Barnacle.
     */
    @Test
    public void testGetBounds() {
        System.out.println("getBounds");
        Enemy_Barnacle instance = new Enemy_Barnacle(ObjectId.Enemy, 0,0);
        Rectangle expResult = new Rectangle(8,24,16,24);
        Rectangle result = instance.getBounds();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsTop method, of class Enemy_Barnacle.
     */
    @Test
    public void testGetBoundsTop() {
        System.out.println("getBoundsTop");
        Enemy_Barnacle instance = new Enemy_Barnacle(ObjectId.Enemy, 30,70);
        Rectangle expResult = new Rectangle(38,70,16,24);
        Rectangle result = instance.getBoundsTop();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsRight method, of class Enemy_Barnacle.
     */
    @Test
    public void testGetBoundsRight() {
        System.out.println("getBoundsRight");
        Enemy_Barnacle instance = new Enemy_Barnacle(ObjectId.Enemy, 30,70);
        Rectangle expResult = new Rectangle(55,74,6,32);
        Rectangle result = instance.getBoundsRight();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsLeft method, of class Enemy_Barnacle.
     */
    @Test
    public void testGetBoundsLeft() {
        System.out.println("getBoundsLeft");
        Enemy_Barnacle instance = new Enemy_Barnacle(ObjectId.Enemy, 30,70);
        Rectangle expResult = new Rectangle(30,74,6,32);
        Rectangle result = instance.getBoundsLeft();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsBottom method, of class Enemy_Barnacle.
     */
    @Test
    public void testGetBoundsBottom() {
        System.out.println("getBoundsBottom");
        Enemy_Barnacle instance =new Enemy_Barnacle(ObjectId.Enemy, 30,70);
        Rectangle expResult = new Rectangle(38,94,16,24);
        Rectangle result = instance.getBoundsBottom();
        assertEquals(expResult, result);
    }

    /**
     * Test of checkCollision method, of class Enemy_Barnacle.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        Enemy_Barnacle instance = new Enemy_Barnacle(ObjectId.Enemy, 30,70);
        instance.checkCollision(objects);
    }
    
}
