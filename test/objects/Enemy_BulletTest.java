/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class Enemy_BulletTest {
    
    public Enemy_BulletTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Enemy_Bullet.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Enemy_Bullet instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class Enemy_Bullet.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Enemy_Bullet instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBounds method, of class Enemy_Bullet.
//     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        Enemy_Bullet instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBoundsLeft method, of class Enemy_Bullet.
//     */
//    @Test
//    public void testGetBoundsLeft() {
//        System.out.println("getBoundsLeft");
//        Enemy_Bullet instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBoundsLeft();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBoundsRight method, of class Enemy_Bullet.
//     */
//    @Test
//    public void testGetBoundsRight() {
//        System.out.println("getBoundsRight");
//        Enemy_Bullet instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBoundsRight();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkCollision method, of class Enemy_Bullet.
     */
    @Test
    public void testCheckCollision_haveIntersectWithPlayer() {
        System.out.println("checkCollision");
        
        
        Player player;
        player = Player.getInstance();
        player.setX(0);
        player.setY(0);
        LinkedList<GameObject> objects = new LinkedList<>();
        objects.add(player);
       
        Enemy_Bullet instance = new Enemy_Bullet(ObjectId.Enemy_Bullet, 5, 5, 1, ObjectId.Enemy);
        try{
            int expectedResult = Player.LIFE-Enemy_Bullet.getDamage();
            instance.checkCollision(objects);
            int actualResult = Player.LIFE;
            assertEquals(expectedResult, actualResult);
        }catch(Exception e){
            
        }
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testCheckCollision_dontHaveIntersectWithPlayer() {
        System.out.println("checkCollision");
        int expectedResult = Player.LIFE;
        
        Player player;
        player = Player.getInstance();
        player.setX(30);
        player.setY(30);
        LinkedList<GameObject> objects = new LinkedList<>();
        objects.add(player);
        
        Enemy_Bullet instance = new Enemy_Bullet(ObjectId.Enemy_Bullet, 20, 20, 1, ObjectId.Enemy);
        instance.checkCollision(objects);
        int actualResult = Player.LIFE;
        assertEquals(expectedResult, actualResult);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    
}
