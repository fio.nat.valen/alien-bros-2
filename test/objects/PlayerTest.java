/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class PlayerTest {
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class Player.
     */
//    @Test
//    public void testGetInstance() {
//        System.out.println("getInstance");
//        Player expResult = null;
//        Player result = Player.getInstance();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of tick method, of class Player.
//     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Player instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of fire method, of class Player.
//     */
//    @Test
//    public void testFire() {
//        System.out.println("fire");
//        Player instance = null;
//        instance.fire();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class Player.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Player instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBounds method, of class Player.
//     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        Player instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBoundsTop method, of class Player.
//     */
//    @Test
//    public void testGetBoundsTop() {
//        System.out.println("getBoundsTop");
//        Player instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBoundsTop();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBoundsRight method, of class Player.
//     */
//    @Test
//    public void testGetBoundsRight() {
//        System.out.println("getBoundsRight");
//        Player instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBoundsRight();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBoundsLeft method, of class Player.
//     */
//    @Test
//    public void testGetBoundsLeft() {
//        System.out.println("getBoundsLeft");
//        Player instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBoundsLeft();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkCollision method, of class Player.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        int expectedResult = 3;
        Player player;
        player = Player.getInstance();
        player.setX(5);
        player.setY(5);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        
        player.checkCollision(objects);
        int actualRes = Player.LIFE;
        assertEquals(expectedResult, actualRes);
    }

    /**
     * Test of getDirection method, of class Player.
     */
//    @Test
//    public void testGetDirection() {
//        System.out.println("getDirection");
//        Player instance = null;
//        int expResult = 0;
//        int result = instance.getDirection();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//    }
//
//    /**
//     * Test of setKey method, of class Player.
//     */
//    @Test
//    public void testSetKey() {
//        System.out.println("setKey");
//        boolean haveKey = false;
//        Player instance = null;
//        instance.setKey(haveKey);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setDirection method, of class Player.
//     */
//    @Test
//    public void testSetDirection() {
//        System.out.println("setDirection");
//        int direction = 0;
//        Player instance = null;
//        instance.setDirection(direction);
//        
//    }

    /**
     * Test of between method, of class Player.
     */
    @Test
    public void testBetween() {
        System.out.println("between");
        int x = 5;
        int a = 1;
        int b = 10;
        boolean expResult = true;
        boolean result = Player.between(x, a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
