/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class Enemies_BossTest {
    
    public Enemies_BossTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Enemies_Boss.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Enemies_Boss instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class Enemies_Boss.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Enemies_Boss instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of fire method, of class Enemies_Boss.
     */
//    @Test
//    public void testFire() {
//        System.out.println("fire");
//        int dir = 0;
//        Enemies_Boss instance = new Enemies_Boss(ObjectId.Enemies_Boss,70,30);
//        instance.fire(dir);
//        
//    }

    /**
     * Test of getBounds method, of class Enemies_Boss.
     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
//        Rectangle expResult = new Rectangle(38,94,16,24);
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getBoundsTop method, of class Enemies_Boss.
     */
    @Test
    public void testGetBoundsTop() {
        System.out.println("getBoundsTop");
        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
        Rectangle expResult = new Rectangle(78,30,16,20);
        Rectangle result = instance.getBoundsTop();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsRight method, of class Enemies_Boss.
     */
    @Test
    public void testGetBoundsRight() {
        System.out.println("getBoundsRight");
        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
        Rectangle expResult = new Rectangle(95,33,6,26);
        Rectangle result = instance.getBoundsRight();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBoundsLeft method, of class Enemies_Boss.
     */
    @Test
    public void testGetBoundsLeft() {
        System.out.println("getBoundsLeft");
        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
        Rectangle expResult = new Rectangle(70,33,6,26);
        Rectangle result = instance.getBoundsLeft();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getBoundsBottom method, of class Enemies_Boss.
     */
    @Test
    public void testGetBoundsBottom() {
        System.out.println("getBoundsBottom");
        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
        Rectangle expResult = new Rectangle(78,50,16,20);
        Rectangle result = instance.getBoundsBottom();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of checkCollision method, of class Enemies_Boss.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        Enemies_Boss instance = new Enemies_Boss(ObjectId.Boss,70,30);
        instance.checkCollision(objects);
      
    }
    
}
